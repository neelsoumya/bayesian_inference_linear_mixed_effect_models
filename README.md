# bayesian_inference_linear_mixed_effect_models

A simple example to perform linear mixed effects regression
in a Bayesian setting using the Edward framework

* Usage:

		python3 edward_bayes_inference_linear_mixed_effects.py

* Adapted from:

		http://edwardlib.org/tutorials/linear-mixed-effects-models

		http://edwardlib.org/tutorials/


